package com.tld.msauthor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsAuthorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsAuthorApplication.class, args);
	}

}
