package com.tld.msarticle.entites;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = "tld-article")
public class ArticleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String content;

    public ArticleEntity(Long id, String title, String content) {
    }

    public String getTitle() {
        return this.title;
    }
}