package com.tld.msarticle.controller;

import com.tld.msarticle.entites.ArticleEntity;
import com.tld.msarticle.repository.ArticleRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ArticleRestController {
    private final ArticleRepository articleRepository;

    public ArticleRestController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @GetMapping("/articles")
    public List<ArticleEntity> list(){
        return articleRepository.findAll();
    }

    @GetMapping("/article/{id}")
    public ArticleEntity getArticle(@PathVariable(required = false) Long id){
        return articleRepository.findById(id).get();
    }

    @PostMapping("/article/add")
    public ArticleEntity save(ArticleEntity article){
        return articleRepository.save(article);
    }

    @DeleteMapping("/article/delete/{id}")
    public void delete(@PathVariable(required = false) Long id){
        articleRepository.deleteById(id);
    }
}
