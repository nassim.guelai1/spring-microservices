package com.tld.msarticle;

import com.tld.msarticle.entites.ArticleEntity;
import com.tld.msarticle.repository.ArticleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@EnableDiscoveryClient
@SpringBootApplication
public class MsArticleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsArticleApplication.class, args);
	}

	@Bean
	CommandLineRunner start(ArticleRepository articleRepository){
		return args ->{
			articleRepository.save(new ArticleEntity(null, "Title 01", "Contain 01"));
			articleRepository.save(new ArticleEntity(null, "Title 02", "Contain 02"));
			articleRepository.findAll().forEach(a->{
				System.out.println(a.getTitle());
			});
		};
	}
}
