package com.tld.msuser.restRepository;

import com.tld.msuser.entites.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "users")
public interface UserRepositoryController extends JpaRepository<UserEntity, Long> {
}
