package com.tld.msuser;

import com.tld.msuser.entites.UserEntity;
import com.tld.msuser.restRepository.UserRepositoryController;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MsUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsUserApplication.class, args);
	}

	@Bean
	CommandLineRunner start(UserRepositoryController userRepositoryController){
		return args ->{
			userRepositoryController.save(new UserEntity(null, "Leo", "Messi"));
			userRepositoryController.save(new UserEntity(null, "Kevin", "Debruyne"));
			userRepositoryController.findAll().forEach(a->{
				System.out.println(a.getNom());
			});
		};
	}
}
